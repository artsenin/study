import java.util.concurrent.*;
class Resource {
	static Semaphore sem = new Semaphore(1);
	static int count;
	static void  setCount() throws InterruptedException {
		System.out.println(Thread.currentThread() + ": waiting for semaphore");
		try {
			sem.acquire();
			count++;
			System.out.println(count);
			sem.release();
		} catch (InterruptedException e ) {
		}
	}
	
}

public class Test {
	public static void main(String[] args)  {
		new Thread (
				() -> {
					try {
						Resource.setCount();
					} catch (InterruptedException e) {
					}
				}
				).start();
	}
}
